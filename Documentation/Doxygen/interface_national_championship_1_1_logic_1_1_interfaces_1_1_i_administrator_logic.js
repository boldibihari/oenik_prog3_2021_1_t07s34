var interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic =
[
    [ "AddClub", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#a38d20fe3e53d4641cc1b4047c09b5231", null ],
    [ "AddManager", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#aab5584da10276a4b41d0b55e1bdba9f7", null ],
    [ "AddManagerToClub", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#acf4cd0d5e74d918901dd03a5fb9257a8", null ],
    [ "AddPlayer", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#aa678a878a69ca4e102e31af15b770e2f", null ],
    [ "AddPlayerToClub", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#ae5936dab8425bfc68e380903fb15bb45", null ],
    [ "DeleteClub", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#a3bf5898d2d915ef559259cdebd8d8312", null ],
    [ "DeleteManager", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#a44a829405b26a3baba3eac8c8d8ada18", null ],
    [ "DeletePlayer", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#a1173c496ae37706e496ec656984a0494", null ],
    [ "UpdateClub", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#aa278ccd665352de847b3f988d6392eab", null ],
    [ "UpdateManager", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#afcdd36795ee1ca4008c6463c6b2cef1a", null ],
    [ "UpdatePlayer", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html#a9194062563a553d4392aebffe53d1ccf", null ]
];