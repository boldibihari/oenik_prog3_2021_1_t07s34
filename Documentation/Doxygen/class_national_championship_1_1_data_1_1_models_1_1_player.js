var class_national_championship_1_1_data_1_1_models_1_1_player =
[
    [ "ToString", "class_national_championship_1_1_data_1_1_models_1_1_player.html#a8987f8b305a04994caddffb316be9d4b", null ],
    [ "Club", "class_national_championship_1_1_data_1_1_models_1_1_player.html#af3da1bdd451d56ff3722a0a5366ac81d", null ],
    [ "ClubId", "class_national_championship_1_1_data_1_1_models_1_1_player.html#afbedc8fb440fe62684e05fbd363b3088", null ],
    [ "PlayerBirthDate", "class_national_championship_1_1_data_1_1_models_1_1_player.html#ac47964a10d178855b0cf10d2ac75bd1b", null ],
    [ "PlayerId", "class_national_championship_1_1_data_1_1_models_1_1_player.html#a29b77192483a458725c3d3e1ce5f420c", null ],
    [ "PlayerName", "class_national_championship_1_1_data_1_1_models_1_1_player.html#addc595a700395d5422cbd6ab0c056794", null ],
    [ "PlayerNationality", "class_national_championship_1_1_data_1_1_models_1_1_player.html#a1b3bdfb88b52a429c0956781281b7ec4", null ],
    [ "PlayerPosition", "class_national_championship_1_1_data_1_1_models_1_1_player.html#ad760326aab043b79ce019e8617292131", null ],
    [ "PlayerValue", "class_national_championship_1_1_data_1_1_models_1_1_player.html#a0bd74b9fa0ba2885c834fd6d1f692cd1", null ]
];