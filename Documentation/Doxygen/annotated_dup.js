var annotated_dup =
[
    [ "NationalChampionship", "namespace_national_championship.html", [
      [ "Data", "namespace_national_championship_1_1_data.html", [
        [ "Models", "namespace_national_championship_1_1_data_1_1_models.html", [
          [ "Club", "class_national_championship_1_1_data_1_1_models_1_1_club.html", "class_national_championship_1_1_data_1_1_models_1_1_club" ],
          [ "Manager", "class_national_championship_1_1_data_1_1_models_1_1_manager.html", "class_national_championship_1_1_data_1_1_models_1_1_manager" ],
          [ "NationalChampionshipDbContext", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context.html", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context" ],
          [ "Player", "class_national_championship_1_1_data_1_1_models_1_1_player.html", "class_national_championship_1_1_data_1_1_models_1_1_player" ]
        ] ]
      ] ],
      [ "Logic", "namespace_national_championship_1_1_logic.html", [
        [ "Classes", "namespace_national_championship_1_1_logic_1_1_classes.html", [
          [ "AdministratorLogic", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic" ],
          [ "Nationality", "class_national_championship_1_1_logic_1_1_classes_1_1_nationality.html", "class_national_championship_1_1_logic_1_1_classes_1_1_nationality" ],
          [ "Position", "class_national_championship_1_1_logic_1_1_classes_1_1_position.html", "class_national_championship_1_1_logic_1_1_classes_1_1_position" ],
          [ "UserLogic", "class_national_championship_1_1_logic_1_1_classes_1_1_user_logic.html", "class_national_championship_1_1_logic_1_1_classes_1_1_user_logic" ]
        ] ],
        [ "Interfaces", "namespace_national_championship_1_1_logic_1_1_interfaces.html", [
          [ "IAdministratorLogic", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic" ],
          [ "IUserLogic", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic" ]
        ] ],
        [ "Tests", "namespace_national_championship_1_1_logic_1_1_tests.html", [
          [ "AdministratorLogicTests", "class_national_championship_1_1_logic_1_1_tests_1_1_administrator_logic_tests.html", "class_national_championship_1_1_logic_1_1_tests_1_1_administrator_logic_tests" ],
          [ "UserLogicTests", "class_national_championship_1_1_logic_1_1_tests_1_1_user_logic_tests.html", "class_national_championship_1_1_logic_1_1_tests_1_1_user_logic_tests" ]
        ] ]
      ] ],
      [ "Repository", "namespace_national_championship_1_1_repository.html", [
        [ "Classes", "namespace_national_championship_1_1_repository_1_1_classes.html", [
          [ "ClubRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository.html", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository" ],
          [ "CommonRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository" ],
          [ "ManagerRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_manager_repository.html", "class_national_championship_1_1_repository_1_1_classes_1_1_manager_repository" ],
          [ "PlayerRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_player_repository.html", "class_national_championship_1_1_repository_1_1_classes_1_1_player_repository" ]
        ] ],
        [ "Interfaces", "namespace_national_championship_1_1_repository_1_1_interfaces.html", [
          [ "IClubRepository", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_club_repository.html", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_club_repository" ],
          [ "ICommonRepository", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository.html", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository" ],
          [ "IManagerRepository", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_manager_repository.html", null ],
          [ "IPlayerRepository", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_player_repository.html", null ]
        ] ]
      ] ],
      [ "AdministratorMenuMethod", "class_national_championship_1_1_administrator_menu_method.html", "class_national_championship_1_1_administrator_menu_method" ],
      [ "Factory", "class_national_championship_1_1_factory.html", "class_national_championship_1_1_factory" ],
      [ "Program", "class_national_championship_1_1_program.html", null ],
      [ "UserMenuMethod", "class_national_championship_1_1_user_menu_method.html", "class_national_championship_1_1_user_menu_method" ]
    ] ]
];