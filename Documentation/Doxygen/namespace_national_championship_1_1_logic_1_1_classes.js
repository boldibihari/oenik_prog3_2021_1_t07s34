var namespace_national_championship_1_1_logic_1_1_classes =
[
    [ "AdministratorLogic", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic" ],
    [ "Nationality", "class_national_championship_1_1_logic_1_1_classes_1_1_nationality.html", "class_national_championship_1_1_logic_1_1_classes_1_1_nationality" ],
    [ "Position", "class_national_championship_1_1_logic_1_1_classes_1_1_position.html", "class_national_championship_1_1_logic_1_1_classes_1_1_position" ],
    [ "UserLogic", "class_national_championship_1_1_logic_1_1_classes_1_1_user_logic.html", "class_national_championship_1_1_logic_1_1_classes_1_1_user_logic" ]
];