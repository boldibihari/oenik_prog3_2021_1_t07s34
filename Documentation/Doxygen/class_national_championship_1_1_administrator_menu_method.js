var class_national_championship_1_1_administrator_menu_method =
[
    [ "AddClub", "class_national_championship_1_1_administrator_menu_method.html#a44b7ec7ab7a31ba9038f16e6f7f4f6d7", null ],
    [ "AddManager", "class_national_championship_1_1_administrator_menu_method.html#aed2cd95d89469b47e85227b287b285cd", null ],
    [ "AddPlayer", "class_national_championship_1_1_administrator_menu_method.html#aa4948a9797907e2721775a27c1c08794", null ],
    [ "DeleteClub", "class_national_championship_1_1_administrator_menu_method.html#aeb03dd7dd9fb1a75ee5568732bacbbef", null ],
    [ "DeleteManager", "class_national_championship_1_1_administrator_menu_method.html#a5428ead22725d88cf2a6aabd340077d4", null ],
    [ "DeletePlayer", "class_national_championship_1_1_administrator_menu_method.html#a8de92a92e7cbdc5bd80f246e7a3a4c33", null ],
    [ "UpdateClub", "class_national_championship_1_1_administrator_menu_method.html#a24d6125d165dc5d56f73a5439f57d66a", null ],
    [ "UpdateManager", "class_national_championship_1_1_administrator_menu_method.html#a3f1405e036afc8acb5dabd17cfc71429", null ],
    [ "UpdatePlayer", "class_national_championship_1_1_administrator_menu_method.html#a16e58f29434028cede0a1bf9573ec623", null ]
];