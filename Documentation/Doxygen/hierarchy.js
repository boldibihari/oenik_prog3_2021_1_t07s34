var hierarchy =
[
    [ "NationalChampionship.Logic.Tests.AdministratorLogicTests", "class_national_championship_1_1_logic_1_1_tests_1_1_administrator_logic_tests.html", null ],
    [ "NationalChampionship.AdministratorMenuMethod", "class_national_championship_1_1_administrator_menu_method.html", null ],
    [ "NationalChampionship.Data.Models.Club", "class_national_championship_1_1_data_1_1_models_1_1_club.html", null ],
    [ "NationalChampionship.Repository.Classes.CommonRepository< Club >", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html", [
      [ "NationalChampionship.Repository.Classes.ClubRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository.html", null ]
    ] ],
    [ "NationalChampionship.Repository.Classes.CommonRepository< Manager >", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html", [
      [ "NationalChampionship.Repository.Classes.ManagerRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_manager_repository.html", null ]
    ] ],
    [ "NationalChampionship.Repository.Classes.CommonRepository< Player >", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html", [
      [ "NationalChampionship.Repository.Classes.PlayerRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_player_repository.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "NationalChampionship.Data.Models.NationalChampionshipDbContext", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context.html", null ]
    ] ],
    [ "NationalChampionship.Factory", "class_national_championship_1_1_factory.html", null ],
    [ "NationalChampionship.Logic.Interfaces.IAdministratorLogic", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_administrator_logic.html", [
      [ "NationalChampionship.Logic.Classes.AdministratorLogic", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html", null ]
    ] ],
    [ "NationalChampionship.Repository.Interfaces.ICommonRepository< T >", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository.html", [
      [ "NationalChampionship.Repository.Classes.CommonRepository< T >", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html", null ]
    ] ],
    [ "NationalChampionship.Repository.Interfaces.ICommonRepository< Club >", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository.html", [
      [ "NationalChampionship.Repository.Interfaces.IClubRepository", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_club_repository.html", [
        [ "NationalChampionship.Repository.Classes.ClubRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository.html", null ]
      ] ]
    ] ],
    [ "NationalChampionship.Repository.Interfaces.ICommonRepository< Manager >", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository.html", [
      [ "NationalChampionship.Repository.Interfaces.IManagerRepository", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_manager_repository.html", [
        [ "NationalChampionship.Repository.Classes.ManagerRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_manager_repository.html", null ]
      ] ]
    ] ],
    [ "NationalChampionship.Repository.Interfaces.ICommonRepository< Player >", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository.html", [
      [ "NationalChampionship.Repository.Interfaces.IPlayerRepository", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_player_repository.html", [
        [ "NationalChampionship.Repository.Classes.PlayerRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_player_repository.html", null ]
      ] ]
    ] ],
    [ "NationalChampionship.Logic.Interfaces.IUserLogic", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html", [
      [ "NationalChampionship.Logic.Classes.UserLogic", "class_national_championship_1_1_logic_1_1_classes_1_1_user_logic.html", null ]
    ] ],
    [ "NationalChampionship.Data.Models.Manager", "class_national_championship_1_1_data_1_1_models_1_1_manager.html", null ],
    [ "NationalChampionship.Logic.Classes.Nationality", "class_national_championship_1_1_logic_1_1_classes_1_1_nationality.html", null ],
    [ "NationalChampionship.Data.Models.Player", "class_national_championship_1_1_data_1_1_models_1_1_player.html", null ],
    [ "NationalChampionship.Logic.Classes.Position", "class_national_championship_1_1_logic_1_1_classes_1_1_position.html", null ],
    [ "NationalChampionship.Program", "class_national_championship_1_1_program.html", null ],
    [ "NationalChampionship.Logic.Tests.UserLogicTests", "class_national_championship_1_1_logic_1_1_tests_1_1_user_logic_tests.html", null ],
    [ "NationalChampionship.UserMenuMethod", "class_national_championship_1_1_user_menu_method.html", null ]
];