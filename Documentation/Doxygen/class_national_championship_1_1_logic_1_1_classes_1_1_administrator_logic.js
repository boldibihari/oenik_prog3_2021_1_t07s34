var class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic =
[
    [ "AdministratorLogic", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#a66f2fd095eeb3b478ec6e1c71a134345", null ],
    [ "AddClub", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#a91b68da252b2e8db8930b3f8ee7bb2ec", null ],
    [ "AddManager", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#a3b1f92983cfb998784ab3aa80f5d6c8f", null ],
    [ "AddManagerToClub", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#ac62612652a74b9e2ec467429f5c60f58", null ],
    [ "AddPlayer", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#a437b72981080f8559a0cb600bfa3d3b8", null ],
    [ "AddPlayerToClub", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#ad813d2a3f0989acfb9194fa23ac8afe2", null ],
    [ "DeleteClub", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#a3c2aa8d1cfed8e1eeb8c45df738937f2", null ],
    [ "DeleteManager", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#ac5458831beb46aad5695e46ee807748a", null ],
    [ "DeletePlayer", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#a161dfbc431cc475617140e63c19a2443", null ],
    [ "UpdateClub", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#a6489fb3dcfa1abfd2803ef0e2c7e2f10", null ],
    [ "UpdateManager", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#a0256930c5886241e8f2eea53bd2c7026", null ],
    [ "UpdatePlayer", "class_national_championship_1_1_logic_1_1_classes_1_1_administrator_logic.html#af7490e7a81508f3a5494c68e5aa94575", null ]
];