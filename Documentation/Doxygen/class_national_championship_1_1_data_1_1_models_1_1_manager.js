var class_national_championship_1_1_data_1_1_models_1_1_manager =
[
    [ "ToString", "class_national_championship_1_1_data_1_1_models_1_1_manager.html#ab797dba3467ea0dbfa44b2275741aea8", null ],
    [ "Club", "class_national_championship_1_1_data_1_1_models_1_1_manager.html#a01e1a4e77919d0d4e6a9307548e7a75d", null ],
    [ "ClubId", "class_national_championship_1_1_data_1_1_models_1_1_manager.html#afc2acf30723bf2926cf39d7804524c92", null ],
    [ "ManagerBirthDate", "class_national_championship_1_1_data_1_1_models_1_1_manager.html#a9a4e8b153846c1b4e9a3f40bdabb935d", null ],
    [ "ManagerId", "class_national_championship_1_1_data_1_1_models_1_1_manager.html#a2e9d868c8300dde7d8bce5e66b4e0b4c", null ],
    [ "ManagerName", "class_national_championship_1_1_data_1_1_models_1_1_manager.html#a12e8cab7da47ed89f3ebd51e1bdbcc36", null ],
    [ "ManagerNationality", "class_national_championship_1_1_data_1_1_models_1_1_manager.html#a0db754abb9cf9e06d2643c1b0fdc4649", null ],
    [ "ManagerStartYear", "class_national_championship_1_1_data_1_1_models_1_1_manager.html#aa75609e6c5642008854ece2d3584ddd9", null ],
    [ "WonChampionship", "class_national_championship_1_1_data_1_1_models_1_1_manager.html#a21a30bbddcf056d6b663f0edf9dfaa2a", null ]
];