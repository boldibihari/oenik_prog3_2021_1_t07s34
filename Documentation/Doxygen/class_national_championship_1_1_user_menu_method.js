var class_national_championship_1_1_user_menu_method =
[
    [ "AverageClubValueAsync", "class_national_championship_1_1_user_menu_method.html#a18a7dc6b20ef629ca697dc691e8535e9", null ],
    [ "GetAllCaptain", "class_national_championship_1_1_user_menu_method.html#aa553cc143fc4713ae357dac63eadc5d4", null ],
    [ "GetAllClub", "class_national_championship_1_1_user_menu_method.html#a7a80e94505694c120287408a2d943155", null ],
    [ "GetAllManager", "class_national_championship_1_1_user_menu_method.html#ac1219232d2e4db64a2b64da1b007b790", null ],
    [ "GetAllPlayer", "class_national_championship_1_1_user_menu_method.html#a4789d42a5b25bfb02ddc97ed2776cc47", null ],
    [ "GetNationalityOneClubAsync", "class_national_championship_1_1_user_menu_method.html#a881f671fb01f98ce3ff1cf5224c194f8", null ],
    [ "GetOneClub", "class_national_championship_1_1_user_menu_method.html#a081264c4fdbeac01e1aa48915a63010e", null ],
    [ "GetOneManager", "class_national_championship_1_1_user_menu_method.html#a19cf42af29b604ba4c76b60cf46ff61c", null ],
    [ "GetOnePlayer", "class_national_championship_1_1_user_menu_method.html#adafa3f875d300c68164d0e60d4a9efa5", null ],
    [ "GetPositionOneClubAsync", "class_national_championship_1_1_user_menu_method.html#ab1cf372fbbc9cecbdaf13915ce3834f1", null ],
    [ "Statistics", "class_national_championship_1_1_user_menu_method.html#a072f9fd95a10c18c35b6fa0e27c2f447", null ]
];