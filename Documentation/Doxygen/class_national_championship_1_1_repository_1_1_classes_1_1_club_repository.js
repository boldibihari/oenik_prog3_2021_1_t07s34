var class_national_championship_1_1_repository_1_1_classes_1_1_club_repository =
[
    [ "ClubRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository.html#a9f5871bd360c8bbd993798e9f039ca78", null ],
    [ "AddManagerToClub", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository.html#ad8728e018580cab146391d70e1c7ebc5", null ],
    [ "AddPlayerToClub", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository.html#ad563b03d3ed1d79efd9fbaf172a732ab", null ],
    [ "GetOne", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository.html#a8e496bcbb8b7473e7f72dbf2f3aa4d48", null ],
    [ "Update", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository.html#a4329d971d9d036359a2c307a01653951", null ]
];