var namespace_national_championship_1_1_data_1_1_models =
[
    [ "Club", "class_national_championship_1_1_data_1_1_models_1_1_club.html", "class_national_championship_1_1_data_1_1_models_1_1_club" ],
    [ "Manager", "class_national_championship_1_1_data_1_1_models_1_1_manager.html", "class_national_championship_1_1_data_1_1_models_1_1_manager" ],
    [ "NationalChampionshipDbContext", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context.html", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context" ],
    [ "Player", "class_national_championship_1_1_data_1_1_models_1_1_player.html", "class_national_championship_1_1_data_1_1_models_1_1_player" ],
    [ "PlayerPosition", "namespace_national_championship_1_1_data_1_1_models.html#a741f7348a30715a02b414422b92acc24", [
      [ "Goalkeeper", "namespace_national_championship_1_1_data_1_1_models.html#a741f7348a30715a02b414422b92acc24a034ba5710292a2a6996139848b42183a", null ],
      [ "Defender", "namespace_national_championship_1_1_data_1_1_models.html#a741f7348a30715a02b414422b92acc24aee90445284a3f5eb04b08417e16ecb95", null ],
      [ "Midfielder", "namespace_national_championship_1_1_data_1_1_models.html#a741f7348a30715a02b414422b92acc24a6eb3d6ece5ad8ff6cc976a96392be1f4", null ],
      [ "Forward", "namespace_national_championship_1_1_data_1_1_models.html#a741f7348a30715a02b414422b92acc24a67d2f6740a8eaebf4d5c6f79be8da481", null ]
    ] ]
];