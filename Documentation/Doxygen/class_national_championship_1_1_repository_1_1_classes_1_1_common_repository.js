var class_national_championship_1_1_repository_1_1_classes_1_1_common_repository =
[
    [ "CommonRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html#a8dd431569264e8cdd520a49d58d89b17", null ],
    [ "Add", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html#a45eddc69b6fd5e64db2698662e618556", null ],
    [ "Delete", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html#aa2d87a41fc6be910f68e1ef26488df05", null ],
    [ "GetAll", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html#a1ea7d4089b6f7987f617ea9417a62f93", null ],
    [ "GetOne", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html#aac28b8f369c6f6d8455768720cdf23bc", null ],
    [ "Update", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html#af1b5c0e12ce48b544a31b826a068afb5", null ],
    [ "context", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html#a6e2d4344f3c34a0d6375b5ee85fa3533", null ]
];