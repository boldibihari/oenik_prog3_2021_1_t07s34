var class_national_championship_1_1_data_1_1_models_1_1_club =
[
    [ "Club", "class_national_championship_1_1_data_1_1_models_1_1_club.html#a8cd9dc92da1e7b3c998cc46660dcb227", null ],
    [ "ToString", "class_national_championship_1_1_data_1_1_models_1_1_club.html#a438276702f099a336aa61270baf8097a", null ],
    [ "ClubCity", "class_national_championship_1_1_data_1_1_models_1_1_club.html#a12783d758e39a9805ba379c55c237391", null ],
    [ "ClubColour", "class_national_championship_1_1_data_1_1_models_1_1_club.html#a5f72119d5b2833065f7d31f23d18d91b", null ],
    [ "ClubFounded", "class_national_championship_1_1_data_1_1_models_1_1_club.html#aba64d312e350b674154318e84d768570", null ],
    [ "ClubId", "class_national_championship_1_1_data_1_1_models_1_1_club.html#ac185d695c72a1447ef962b4a28410b82", null ],
    [ "ClubName", "class_national_championship_1_1_data_1_1_models_1_1_club.html#ade57a6ee90b0b904168f27121b075f24", null ],
    [ "Manager", "class_national_championship_1_1_data_1_1_models_1_1_club.html#ab09be24ee167bbfcb25fcf922e52cab5", null ],
    [ "Players", "class_national_championship_1_1_data_1_1_models_1_1_club.html#a7a9bf321efe7f0d19605a5b4dba482b8", null ],
    [ "Stadium", "class_national_championship_1_1_data_1_1_models_1_1_club.html#a3680a14b746d8a3661041424e7672a50", null ]
];