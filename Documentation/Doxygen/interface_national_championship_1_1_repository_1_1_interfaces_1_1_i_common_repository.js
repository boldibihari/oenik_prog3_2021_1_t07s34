var interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository =
[
    [ "Add", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository.html#a265b3f68f4e2bff21957639a95d31a33", null ],
    [ "Delete", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository.html#ac4b26e4f1ffe7e47a7bd7a764ae4fa74", null ],
    [ "GetAll", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository.html#a1beffe2627e65e6e1ad11bd4cba0910c", null ],
    [ "GetOne", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository.html#a43ace625095968a43874b5c59f9e188b", null ],
    [ "Update", "interface_national_championship_1_1_repository_1_1_interfaces_1_1_i_common_repository.html#a625f277759a21be6cbd0a7a8c84e3250", null ]
];