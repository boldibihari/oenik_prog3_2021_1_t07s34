var interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic =
[
    [ "AllAverageAge", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a2f264dec34c39f4d21c7646dbd772eb1", null ],
    [ "AllValue", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#ad410ace34f4435a1a328a542f04c78e8", null ],
    [ "AverageClubValue", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a5ca5021d83b4bb4e847b9be2bb5a571f", null ],
    [ "AverageClubValueAsync", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a48524599ea00375b701bfbaff7dd4990", null ],
    [ "AveragePlayerValue", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a659cc739a57bf254f225340af7e9fd76", null ],
    [ "ClubAverageAge", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a4af8acc38247ddac4c65145f694312dd", null ],
    [ "ClubAveragePlayerValue", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#aeefc9b3ff210391d7fed11ffb262c40c", null ],
    [ "ClubValue", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#ad00ef432215292abcf69040b3fee6ecb", null ],
    [ "GetAllCaptain", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a9b257ff31313a7a99ca4db3d67e18574", null ],
    [ "GetAllClub", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#ab7def30c023fbf9415cfeae4e59d786b", null ],
    [ "GetAllManager", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a1d947c487629f242989178362300867e", null ],
    [ "GetAllNationality", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a17d721601f8c3a625a4607fa3948a109", null ],
    [ "GetAllPlayer", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a1e15bb90afcba695d8e3493a5249164a", null ],
    [ "GetAllPosition", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a367d9f4ff26842f585c70761b08543e7", null ],
    [ "GetNationalityOneClub", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#adb4041e3f4585a25e781387f8abb1560", null ],
    [ "GetNationalityOneClubAsync", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#ac4ef39ceb3421da0bf715a1d1cf356d0", null ],
    [ "GetOneClub", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a0da2aff6cc24e65e6ad9dbf6cf6ac85a", null ],
    [ "GetOneManager", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#ae9f0a4abaceeaaad90d61bd03086ae00", null ],
    [ "GetOnePlayer", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#a709ddaf308954975e323879ad2109e26", null ],
    [ "GetPositionOneClub", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#aab3469e04d2f47a544411d5f9b306312", null ],
    [ "GetPositionOneClubAsync", "interface_national_championship_1_1_logic_1_1_interfaces_1_1_i_user_logic.html#aa6d5f6e721351200dd9ddba1fa6a31bc", null ]
];