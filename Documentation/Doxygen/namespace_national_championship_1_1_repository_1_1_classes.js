var namespace_national_championship_1_1_repository_1_1_classes =
[
    [ "ClubRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository.html", "class_national_championship_1_1_repository_1_1_classes_1_1_club_repository" ],
    [ "CommonRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository.html", "class_national_championship_1_1_repository_1_1_classes_1_1_common_repository" ],
    [ "ManagerRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_manager_repository.html", "class_national_championship_1_1_repository_1_1_classes_1_1_manager_repository" ],
    [ "PlayerRepository", "class_national_championship_1_1_repository_1_1_classes_1_1_player_repository.html", "class_national_championship_1_1_repository_1_1_classes_1_1_player_repository" ]
];