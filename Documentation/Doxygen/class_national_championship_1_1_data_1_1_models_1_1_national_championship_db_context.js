var class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context =
[
    [ "NationalChampionshipDbContext", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context.html#adad20cbf1d80b693cff14d91d621699c", null ],
    [ "NationalChampionshipDbContext", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context.html#ab38807aada410a975c90ed353dcce085", null ],
    [ "OnConfiguring", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context.html#a28ba0e6dc3f47d6f24b9c269465761c9", null ],
    [ "OnModelCreating", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context.html#a74e54066c412d51ebb7c194b87817f1f", null ],
    [ "Clubs", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context.html#a775a6b879277b029da51bcea569933f2", null ],
    [ "Managers", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context.html#a4b2e8919ca67265d8e71d78dd0d203fd", null ],
    [ "Players", "class_national_championship_1_1_data_1_1_models_1_1_national_championship_db_context.html#a9a5e440ca8e4469e58543e9081e87572", null ]
];